package linkedlist

import "testing"

func TestLinked_should_add_one_elements(t *testing.T) {

	linkedList := new(LinkedList)
	linkedList.Add("A")

	if linkedList.Len() != 1 {
		t.Errorf("The size of this linkedlist is not 1 . %v", linkedList)
	}

}

func TestLinked_should_have_zero_length(t *testing.T) {
	linkedList := new(LinkedList)

	if linkedList.IsNotEmpty() {
		t.Errorf("The size of this linkedlist is not empty . %v", linkedList)
	}
}

func TestLinked_should_add_two_elements(t *testing.T) {
	linkedList := new(LinkedList)
	linkedList.Add("A")
	linkedList.Add("B")

	if linkedList.Len() != 2 {
		t.Errorf("The size of this linkedlist is not 2 . %v", linkedList)
	}

	if linkedList.RealSize() != 2 {
		t.Errorf("The size of this linkedlist is not 2 . %v", linkedList)
	}
}

func TestLinked_should_remove_one_element_and_get_empty_linkedin(t *testing.T) {
	linkedList := new(LinkedList)
	linkedList.Add("Value to delete")

	if linkedList.Len() != 1 {
		t.Errorf("The size of this linkedlist is not 1 . %v", linkedList)
	}

	linkedList.Remove("Value to delete")

	if linkedList.IsNotEmpty() {
		t.Errorf("The size of this linkedlist is not 0 . %v", linkedList)
	}

	if linkedList.RealSize() != 0 {
		t.Errorf("The size of this linkedlist is not 0 . %v", linkedList)
	}
}

func TestLinked_should_remove_one_element_of_four(t *testing.T) {

	linkedList := new(LinkedList)
	linkedList.Add("A")
	linkedList.Add("B")
	linkedList.Add("C")
	linkedList.Add("D")
	linkedList.Add("E")

	if linkedList.Len() != 5 {
		t.Errorf("The size of this linkedlist is not 5 . %v", linkedList)
	}

	linkedList.Remove("A")

	if linkedList.Len() != 4 {
		t.Errorf("The size of this linkedlist is not 4 . %v", linkedList)
	}

	if linkedList.RealSize() != 4 {
		t.Errorf("The size of this linkedlist is not 4 . %v", linkedList)
	}

}

func TestLinked_remove_all_elements(t *testing.T) {

	linkedList := new(LinkedList)
	linkedList.Add("A")
	linkedList.Add("B")
	linkedList.Add("C")
	linkedList.Add("D")
	linkedList.Add("E")

	if linkedList.Len() != 5 {
		t.Errorf("The size of this linkedlist is not 5 . %v", linkedList)
	}

	linkedList.Clean()

	if linkedList.Len() != 0 {
		t.Errorf("The size of this linkedlist is not 4 . %v", linkedList)
	}
	if linkedList.IsNotEmpty() {
		t.Errorf("The size of this linkedlist is not 4 . %v", linkedList)
	}

	if linkedList.RealSize() != 0 {
		t.Errorf("The size of this linkedlist is not 4 . %v", linkedList)
	}

}

func TestLinked_should_remove_one_element(t *testing.T) {

	linkedList := new(LinkedList)
	linkedList.Add("A")
	linkedList.Add("B")
	linkedList.Add("C")
	linkedList.Add("D")
	linkedList.Add("E")
	linkedList.Add(1)

	if linkedList.Len() != 6 {
		t.Errorf("The size of this linkedlist is not 6 . %v", linkedList)
	}

	linkedList.Remove(1)

	if linkedList.Len() != 5 {
		t.Errorf("The size of this linkedlist is not 5 . %v", linkedList)
	}

	if linkedList.RealSize() != 5 {
		t.Errorf("The size of this linkedlist is not 5 . %v", linkedList)
	}

}
