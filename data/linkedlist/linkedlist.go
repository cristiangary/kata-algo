package linkedlist

type node struct {
	next  *node
	value interface{}
}

type LinkedList struct {
	head   *node
	tail   *node
	length int
}

func (l *LinkedList) Add(value interface{}) {

	node := &node{value: value, next: nil}

	if l.head == nil {
		l.head = node
		l.tail = node

	} else {
		l.tail.next = node
		l.tail = node
	}

	l.length++
}

func (l *LinkedList) Len() int {
	return l.length
}

func (l *LinkedList) IsEmpty() bool {
	return l.length == 0
}
func (l *LinkedList) IsNotEmpty() bool {
	return !l.IsEmpty()
}

func (l *LinkedList) RealSize() int {
	count := 0
	head := l.head

	for ; head != nil; head = head.next {
		count++
	}
	return count
}

func (l *LinkedList) Remove(value interface{}) {

	prev := l.head
	head := l.head

	if l.head.value == value {
		l.head = l.head.next
		l.length--
		return
	}

	for ; head != nil; head = head.next {
		if head.value == value {
			prev.next = head.next
			head.value = nil
			l.length--
		}
		prev = head
	}
}

func (l *LinkedList) Clean() {

	if l.length == 1 {
		l.head = nil
		l.tail = nil
		return
	}

	prev := l.head
	head := l.head.next

	for ; head.next != nil; head = head.next {
		prev.next = nil
		prev = head
	}
	l.head = nil
	l.tail = nil
	l.length = 0
}
