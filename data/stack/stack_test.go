package stack

import (
	"strconv"
	"testing"
)

func TestStack_should_add_one_element(t *testing.T) {
	stack := new(Stack)

	stack.Push("A")

	if stack.Size() != 1 {
		t.Errorf("This stack size is not 1 . %v", stack)
	}
}

func TestStack_should_pop_one_element(t *testing.T) {
	stack := new(Stack)

	stack.Push("A")

	if stack.Size() != 1 {
		t.Errorf("This stack size is not 1 . %v", stack)
	}

	value := stack.Pop()

	if stack.Size() != 0 {
		t.Errorf("This stack size is not 0 . %v", stack)
	}

	if value != "A" {
		t.Errorf("The element pop is not  . %v", value)
	}
}

func TestStack_should_push_and_pop_in_order_lifo(t *testing.T) {

	values := []int{7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 6, 5, 4, 3, 2, 1}

	stack := new(Stack)

	for i := len(values) - 1; i >= 0; i-- {
		stack.Push(values[i])
	}

	if stack.Size() != len(values) {
		t.Errorf("This stack size is not %d . %v", len(values), stack)
	}

	for _, value := range values {
		if stack.Pop() != value {
			t.Errorf("The element pop is not  . %v", value)
		}
	}

	if stack.Size() != 0 {
		t.Errorf("The stack is not empty  . %v", stack)
	}
}

func TestStack_should_return_nil_when_stack_is_empty(t *testing.T) {

	stack := new(Stack)

	if stack.Pop() != nil {
		t.Errorf("The stack didn't return nil value . %v", stack)
	}

}

func TestStack_should_push_and_pop_in_structs(t *testing.T) {

	var values []interface{}

	for i := 0; i < 10; i++ {
		value := struct {
			value int
			name  string
		}{
			value: i,
			name:  "name-" + strconv.Itoa(i),
		}
		values = append(values, value)
	}
	stack := new(Stack)

	for i := len(values) - 1; i >= 0; i-- {
		stack.Push(values[i])
	}

	if stack.Size() != len(values) {
		t.Errorf("This stack size is not %d . %v", len(values), stack)
	}

	for _, value := range values {
		if stack.Pop() != value {
			t.Errorf("The element pop is not  . %v", value)
		}
	}

	if stack.Size() != 0 {
		t.Errorf("The stack is not empty  . %v", stack)
	}
}
