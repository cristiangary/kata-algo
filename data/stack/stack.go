package stack

type Stack struct {
	data []interface{}
}

func (s *Stack) Push(value interface{}) {
	s.data = append(s.data, value)
}

func (s *Stack) Size() int {
	return len(s.data)
}

func (s *Stack) Pop() interface{} {

	if len(s.data) == 0 {
		return nil
	}

	elements := s.data[len(s.data)-1:]

	s.data = s.data[:len(s.data)-1]

	return elements[0]
}
