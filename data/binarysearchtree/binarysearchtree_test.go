package binarysearchtree

import "testing"

func TestBST_should_put_one_element(t *testing.T) {
	bst := new(BinarySearchTree)

	bst.Add(10)

	if bst.Size() != 1 {
		t.Errorf("The size is not 1")
	}

	if bst.RealSize() != 1 {
		t.Errorf("The size is not 1")
	}
}

func TestBST_should_put_two_element(t *testing.T) {
	bst := new(BinarySearchTree)

	bst.Add(10)
	bst.Add(12)

	if bst.Size() != 2 {
		t.Errorf("The size is not 2")
	}

	if bst.RealSize() != 2 {
		t.Errorf("The size is not 2")
	}
}

func TestBST_should_put_tree_element(t *testing.T) {
	bst := new(BinarySearchTree)

	bst.Add(10)
	bst.Add(12)
	bst.Add(5)

	if bst.Size() != 3 {
		t.Errorf("The size is not 3")
	}

	if bst.RealSize() != 3 {
		t.Errorf("The size is not 3")
	}
}

func TestBST_should_put_five_element(t *testing.T) {
	bst := new(BinarySearchTree)

	bst.Add(10)
	bst.Add(12)
	bst.Add(5)
	bst.Add(1)
	bst.Add(20)

	if bst.Size() != 5 {
		t.Errorf("The size is not 5")
	}

	if bst.RealSize() != 5 {
		t.Errorf("The size is not 5")
	}
}
