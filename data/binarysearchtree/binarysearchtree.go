package binarysearchtree

import "kata-algo/data/queue"

type node struct {
	leftChild  *node
	rightChild *node
	value      int
}

type BinarySearchTree struct {
	root *node
	size int
}

func (t *BinarySearchTree) Add(value int) {
	newNode := &node{value: value, leftChild: nil, rightChild: nil}

	if t.root == nil {
		t.root = newNode
		t.size++
		return
	}

	queueDS := new(queue.Queue)
	queueDS.Enqueue(t.root)

	for queueDS.Size() != 0 {

		r := queueDS.Dequeue().(*node)

		if r.value > value {
			if r.leftChild == nil {
				r.leftChild = newNode
				t.size++
				return
			} else {
				queueDS.Enqueue(r.leftChild)
			}
		}
		if r.value < value {
			if r.rightChild == nil {
				r.rightChild = newNode
				t.size++
				return
			} else {
				queueDS.Enqueue(r.rightChild)
			}
		}
	}
}

func (t *BinarySearchTree) Size() int {
	return t.size
}

func (t *BinarySearchTree) RealSize() int {
	size := 0

	count(t.root, &size)

	return size
}

func count(node *node, size *int) {
	if node == nil {
		return
	}
	*size++
	count(node.leftChild, size)
	count(node.rightChild, size)

}
