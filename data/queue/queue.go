package queue

type Queue struct {
	data []interface{}
}

func (q *Queue) Enqueue(value interface{}) {
	q.data = append(q.data, value)
}

func (q *Queue) Size() int {
	return len(q.data)
}

func (q *Queue) Dequeue() interface{} {

	if len(q.data) == 0 {
		return nil
	}
	data := q.data[0]
	q.data = q.data[1:]

	return data
}
