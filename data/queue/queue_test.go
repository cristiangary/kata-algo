package queue

import "testing"

func TestQueue_should_put_one_element_in_queue(t *testing.T) {
	queue := new(Queue)

	queue.Enqueue("A")

	if queue.Size() != 1 {
		t.Errorf("Queue size is different from 1")
	}
}

func TestQueue_should_put_two_element_in_queue(t *testing.T) {
	queue := new(Queue)

	queue.Enqueue("A")
	queue.Enqueue(1)

	if queue.Size() != 2 {
		t.Errorf("Queue size is different from 2")
	}
}

func TestQueue_should_pop_element_from_the_queue(t *testing.T) {
	queue := new(Queue)
	queue.Enqueue(1)

	if queue.Size() != 1 {
		t.Errorf("Queue size is different from 1")
	}

	element := queue.Dequeue()

	if queue.Size() != 0 {
		t.Errorf("Queue size is different from 0")
	}

	if element != 1 {
		t.Errorf("The element dequeue is not %v", element)
	}

}

func TestQueue_should_get_the_element_fifo(t *testing.T) {
	queue := new(Queue)
	queue.Enqueue(1)
	queue.Enqueue("A")
	queue.Enqueue("B")

	if queue.Size() != 3 {
		t.Errorf("Queue size is different from 3")
	}

	element := queue.Dequeue()
	if element != 1 {
		t.Errorf("The elemnt is not the first, %v", element)
	}

	if queue.Size() != 2 {
		t.Errorf("Queue size is different from 2")
	}
}

func TestQueue_should_get_nil_value_when_queue_is_empty(t *testing.T) {
	queue := new(Queue)

	element := queue.Dequeue()
	if element != nil {
		t.Errorf("The elemnt is not nil")
	}

}

func TestQueue_should_push_and_pop_in_order_fifo(t *testing.T) {

	values := []int{7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 6, 5, 4, 3, 2, 1}

	queue := new(Queue)

	for _, value := range values {
		queue.Enqueue(value)
	}

	if queue.Size() != len(values) {
		t.Errorf("This queue size is not %d . %v", len(values), queue)
	}

	for _, value := range values {
		if queue.Dequeue() != value {
			t.Errorf("The element pop is not  . %v", value)
		}
	}

	if queue.Size() != 0 {
		t.Errorf("The queue is not empty  . %v", queue)
	}
}
