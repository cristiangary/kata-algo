package binarytree

import (
	"kata-algo/data/queue"
)

type node struct {
	leftChild  *node
	rightChild *node
	value      interface{}
}

type BinaryTree struct {
	root *node
	size int
}

func (t *BinaryTree) Add(value interface{}) {
	newNode := &node{value: value, leftChild: nil, rightChild: nil}

	if t.root == nil {
		t.root = newNode
		t.size++
		return
	}

	queueDS := new(queue.Queue)
	queueDS.Enqueue(t.root)

	for queueDS.Size() != 0 {

		tempNode := queueDS.Dequeue().(*node)

		if tempNode.leftChild == nil {
			tempNode.leftChild = newNode
			t.size++
			break
		} else {
			queueDS.Enqueue(tempNode.leftChild)
		}

		if tempNode.rightChild == nil {
			tempNode.rightChild = newNode
			t.size++
			break
		} else {
			queueDS.Enqueue(tempNode.rightChild)
		}
	}

}

func (t *BinaryTree) Size() int {
	return t.size
}

func (t *BinaryTree) RealSize() int {

	size := 0

	count(t.root, &size)

	return size
}

func count(node *node, size *int) {
	if node == nil {
		return
	}
	*size++
	count(node.leftChild, size)
	count(node.rightChild, size)

}

func (t *BinaryTree) Remove(value interface{}) {

	if t.root.leftChild == nil && t.root.rightChild == nil && t.root.value == value {
		t.root = nil
		t.size = 0
		return
	}

	queueDS := new(queue.Queue)
	queueDS.Enqueue(t.root)

	var keyNode *node = nil
	var tempNode *node = nil

	for queueDS.Size() != 0 {
		tempNode = queueDS.Dequeue().(*node)

		if tempNode.value == value {
			keyNode = tempNode
		}

		if tempNode.leftChild != nil {
			queueDS.Enqueue(tempNode.leftChild)
		}

		if tempNode.rightChild != nil {
			queueDS.Enqueue(tempNode.rightChild)
		}
	}

	if keyNode != nil && tempNode != nil {
		value := tempNode.value
		t.deleteDeepest(tempNode)
		keyNode.value = value
		t.size--
	}
}

func (t *BinaryTree) deleteDeepest(dNode *node) {
	queueDS := new(queue.Queue)
	queueDS.Enqueue(t.root)

	for queueDS.Size() != 0 {
		tempNode := queueDS.Dequeue().(*node)

		if tempNode == dNode {
			tempNode = nil
			return
		}

		if tempNode.rightChild != nil {

			if tempNode.rightChild == dNode {
				tempNode.rightChild = nil
			} else {
				queueDS.Enqueue(tempNode.rightChild)
			}
		}

		if tempNode.leftChild != nil {
			if tempNode.leftChild == dNode {
				tempNode.leftChild = nil
			} else {
				queueDS.Enqueue(tempNode.leftChild)
			}
		}

	}
}

func (t *BinaryTree) Search(s interface{}) interface{} {

	if t.root.value == s {
		return t.root.value
	}
	queueDS := new(queue.Queue)
	queueDS.Enqueue(t.root)

	for queueDS.Size() != 0 {
		node := queueDS.Dequeue().(*node)

		if node.value == s {
			return node.value
		}

		if node.leftChild != nil {
			queueDS.Enqueue(node.leftChild)
		}
		if node.rightChild != nil {
			queueDS.Enqueue(node.rightChild)
		}

	}

	return nil
}
