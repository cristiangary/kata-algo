package binarytree

import "testing"

func TestBinaryTree_should_put_one_element_in_the_tree(t *testing.T) {

	tree := new(BinaryTree)

	tree.Add("A")

	if tree.Size() != 1 {
		t.Errorf("The tree dont have size 1")
	}
}

func TestBinaryTree_should_put_two_element_in_the_tree(t *testing.T) {

	tree := new(BinaryTree)

	tree.Add("A")
	tree.Add("B")

	if tree.Size() != 2 {
		t.Errorf("The tree dont have size 2")
	}
}

func TestBinaryTree_should_put_three_element_in_the_tree(t *testing.T) {

	tree := new(BinaryTree)

	tree.Add("A")
	tree.Add("B")
	tree.Add("C")

	if tree.Size() != 3 {
		t.Errorf("The tree dont have size 3")
	}
}

func TestBinaryTree_should_put_four_element_in_the_tree(t *testing.T) {

	tree := new(BinaryTree)

	tree.Add("A")
	tree.Add("B")
	tree.Add("C")
	tree.Add("D")

	if tree.Size() != 4 {
		t.Errorf("The tree dont have size 4")
	}
}

func TestBinaryTree_should_get_real_size(t *testing.T) {

	tree := new(BinaryTree)

	tree.Add("A")
	tree.Add("B")
	tree.Add("C")
	tree.Add("D")

	if tree.Size() != 4 {
		t.Errorf("The tree dont have size 4")
	}

	if tree.RealSize() != 4 {
		t.Errorf("The tree don't have real size 4")
	}
}

func TestBinaryTree_should_remove_the_deepest_left_element(t *testing.T) {
	tree := new(BinaryTree)

	tree.Add("A")
	tree.Add("B")
	tree.Add("C")
	tree.Add("D")

	if tree.Size() != 4 {
		t.Errorf("The tree dont have size 4")
	}

	tree.Remove("D")

	if tree.Size() != 3 {
		t.Errorf("The tree dont have size 3")
	}

	if tree.RealSize() != 3 {
		t.Errorf("The tree don't have real size 3")
	}

}

func TestBinaryTree_should_remove_the_deepest_right_element(t *testing.T) {
	tree := new(BinaryTree)

	tree.Add("A")
	tree.Add("B")
	tree.Add("C")
	tree.Add("D")

	if tree.Size() != 4 {
		t.Errorf("The tree dont have size 4")
	}

	tree.Remove("C")

	if tree.Size() != 3 {
		t.Errorf("The tree dont have size 3")
	}

	if tree.RealSize() != 3 {
		t.Errorf("The tree don't have real size 3")
	}

}

func TestBinaryTree_should_remove_the_root_element(t *testing.T) {
	tree := new(BinaryTree)

	tree.Add("A")
	tree.Add("B")
	tree.Add("C")
	tree.Add("D")

	if tree.Size() != 4 {
		t.Errorf("The tree dont have size 4")
	}

	tree.Remove("A")

	if tree.Size() != 3 {
		t.Errorf("The tree dont have size 3")
	}

	if tree.RealSize() != 3 {
		t.Errorf("The tree don't have real size 3")
	}

}

func TestBinaryTree_should_remove_the_middle_root_element(t *testing.T) {
	tree := new(BinaryTree)

	tree.Add("A")
	tree.Add("B")
	tree.Add("C")
	tree.Add("D")

	if tree.Size() != 4 {
		t.Errorf("The tree dont have size 4")
	}

	tree.Remove("B")

	if tree.Size() != 3 {
		t.Errorf("The tree dont have size 3")
	}

	if tree.RealSize() != 3 {
		t.Errorf("The tree don't have real size 3")
	}

}

func TestBinaryTree_should_search_the_middle_root_element(t *testing.T) {
	tree := new(BinaryTree)

	tree.Add("A")
	tree.Add("B")
	tree.Add("C")
	tree.Add("D")

	if tree.Size() != 4 {
		t.Errorf("The tree dont have size 4")
	}

	element := tree.Search("B")

	if tree.Size() != 4 {
		t.Errorf("The tree dont have size 3")
	}

	if tree.RealSize() != 4 {
		t.Errorf("The tree don't have real size 3")
	}

	if element != "B" {
		t.Errorf("The element is not B")
	}

}

func TestBinaryTree_should_search_root_element(t *testing.T) {
	tree := new(BinaryTree)

	tree.Add("A")
	tree.Add("B")
	tree.Add("C")
	tree.Add("D")

	if tree.Size() != 4 {
		t.Errorf("The tree dont have size 4")
	}

	element := tree.Search("A")

	if tree.Size() != 4 {
		t.Errorf("The tree dont have size 3")
	}

	if tree.RealSize() != 4 {
		t.Errorf("The tree don't have real size 3")
	}

	if element != "A" {
		t.Errorf("The element is not A")
	}

}
