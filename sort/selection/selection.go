package selection

import "math"

func sort(array []int) []int {
	if len(array) == 0 {
		return array
	}

	for i := 0; i < len(array); i++ {
		var temp = math.MaxInt32
		var index = -1
		for j := i; j < len(array); j++ {
			if array[j] < temp {
				temp = array[j]
				index = j
			}
		}
		array[i], array[index] = temp, array[i]

	}

	return array
}
