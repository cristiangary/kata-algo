package selection

import (
	"math/rand"
	"reflect"
	"testing"
	"time"
)

func TestSort_should_return_empty_slice(t *testing.T) {

	var unsortedArray []int

	sortedArray := sort(unsortedArray)

	if len(sortedArray) != 0 {
		t.Error("The array is not empty")
		t.FailNow()
	}
}

func TestSort_should_sort_the_input_array(t *testing.T) {
	expectedArray := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	unsortedArray := []int{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}

	sortedArray := sort(unsortedArray)

	if !reflect.DeepEqual(sortedArray, expectedArray) {
		t.Errorf("This array is not sorted . %v", sortedArray)
	}
}

func TestSort_should_return_nil_when_the_input_is_nil(t *testing.T) {
	sortedArray := sort(nil)
	if sortedArray != nil {
		t.Errorf(" the result array should be nil. %v", sortedArray)
	}
}

func TestSort_should_return_sorted_array_of_one_element(t *testing.T) {
	expectedArray := []int{1}
	unsortedArray := []int{1}

	sortedArray := sort(unsortedArray)

	if !reflect.DeepEqual(sortedArray, expectedArray) {
		t.Errorf("This array is not sorted . %v", sortedArray)
	}
}

func BenchmarkHeap_with_randoms(b *testing.B) {
	rand.Seed(time.Now().Unix())
	array := rand.Perm(10_000_000)
	for i := 0; i < b.N; i++ {
		sort(array)
	}
}
