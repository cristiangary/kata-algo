package quick

func sort(array []int) []int {
	quicksort(array, 0, len(array)-1)
	return array
}

func partition(array []int, low int, high int) int {

	i := low - 1
	pivot := array[high]

	for j := low; j < high; j++ {
		if array[j] < pivot {
			i++
			array[i], array[j] = array[j], array[i]
		}
	}
	array[i+1], array[high] = array[high], array[i+1]

	return i + 1
}

func quicksort(array []int, low int, high int) {
	if low >= high {
		return
	}

	pi := partition(array, low, high)

	quicksort(array, low, pi-1)
	quicksort(array, pi+1, high)
}
