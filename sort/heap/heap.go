package heap

func sort(array []int) []int {

	size := len(array)

	for i := size/2 - 1; i >= 0; i-- {
		heapify(array, size, i)
	}

	for i := size - 1; i > 0; i-- {
		array[0], array[i] = array[i], array[0]
		heapify(array, 0, i)
	}

	return array
}

func heapify(list []int, root int, size int) {
	largest := root
	left := 2*root + 1
	right := 2*root + 2

	if left < size && list[left] > list[largest] {
		largest = left
	}

	if right < size && list[right] > list[largest] {
		largest = right
	}
	if largest != root {
		list[root], list[largest] = list[largest], list[root]
		heapify(list, largest, size)
	}
}
