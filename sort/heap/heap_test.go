package heap

import (
	"math/rand"
	"reflect"
	"testing"
	"time"
)

func TestSort_should_return_empty_array_when_the_array_is_empty(t *testing.T) {

	var unsortedArray []int

	sortedArray := sort(unsortedArray)

	if len(sortedArray) != 0 {
		t.Error("The array is not empty")
		t.FailNow()
	}

}

func BenchmarkHeap_with_randoms(b *testing.B) {
	rand.Seed(time.Now().Unix())
	array := rand.Perm(10_000_000)
	for i := 0; i < b.N; i++ {
		sort(array)
	}
}

func BenchmarkHeap_with_10_elements(b *testing.B) {
	for i := 0; i < b.N; i++ {
		unsortedArray := []int{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}
		sort(unsortedArray)
	}
}

func TestSort_should_sort_the_input_array(t *testing.T) {
	expectedArray := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	unsortedArray := []int{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}

	sortedArray := sort(unsortedArray)

	if !reflect.DeepEqual(sortedArray, expectedArray) {
		t.Errorf("This array is not sorted . %v", sortedArray)
	}
}

func TestSort_should_return_nil_when_the_input_is_nil(t *testing.T) {
	sortedArray := sort(nil)
	if sortedArray != nil {
		t.Errorf(" the result array should be nil. %v", sortedArray)
	}
}

func TestSort_should_return_sorted_array_of_one_element(t *testing.T) {
	expectedArray := []int{1}
	unsortedArray := []int{1}

	sortedArray := sort(unsortedArray)

	if !reflect.DeepEqual(sortedArray, expectedArray) {
		t.Errorf("This array is not sorted . %v", sortedArray)
	}
}

func TestHeapify_should_build_heap_tree(t *testing.T) {
	list := []int{4, 10, 3, 5, 1}
	expected := []int{10, 5, 3, 4, 1}

	heapify(list, 0, len(list))

	if !reflect.DeepEqual(expected, list) {
		t.Errorf("This array is not heapify . %v", list)
	}

}
