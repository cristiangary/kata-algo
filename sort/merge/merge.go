package merge

func sort(array []int) []int {
	if len(array) == 0 {
		return array
	}

	return mergesort(array)
}

func mergesort(array []int) []int {
	if len(array) == 1 {
		return array
	}
	list1 := mergesort(array[0:(len(array) / 2)])
	list2 := mergesort(array[(len(array) / 2):])

	return merge(list1, list2)

}

func merge(list1 []int, list2 []int) []int {
	result := make([]int, len(list1)+len(list2))

	k := 0
	i := 0
	j := 0

	for i < len(list1) && j < len(list2) {
		if list1[i] < list2[j] {
			result[k] = list1[i]
			i++
		} else {
			result[k] = list2[j]
			j++
		}
		k++
	}

	for i < len(list1) {
		result[k] = list1[i]
		i++
		k++
	}
	for j < len(list2) {
		result[k] = list2[j]
		j++
		k++
	}

	return result
}
