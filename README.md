KATA Golang [![pipeline status](https://gitlab.com/cristiangary/kata-algo/badges/master/pipeline.svg)](https://gitlab.com/cristiangary/kata-algo/-/commits/master)

# SORT

* ### Insertion 
* ### Selection 
* ### Merge
* ### Heap
* ### Quick 


# Data
* ### Linked List
* ### Queue
* ### Stack
* ### Binary tree
* ### Binary Search Tree
